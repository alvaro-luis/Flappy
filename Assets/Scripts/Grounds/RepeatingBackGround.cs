﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackGround : MonoBehaviour
{
	private BoxCollider2D groundCollider;
	private float groundHorizontalLenght;

	private Vector2 reposicion;

	void Awake()
	{
		groundCollider = GetComponent<BoxCollider2D>();
	}

	// Use this for initialization
	void Start ()
    {
        GetRepositioningPosition();
    }    

    // Update is called once per frame
    void Update () 
	{
		if (transform.position.x < -groundHorizontalLenght)
		{
			RepositionBackGround();
		}
	}

	private void GetRepositioningPosition()
    {
        groundCollider = GetComponent<BoxCollider2D>();
        groundHorizontalLenght = groundCollider.size.x;
        reposicion = new Vector2(groundHorizontalLenght * 2, 0);
    }

	void RepositionBackGround()
	{
		transform.Translate(reposicion);
		//transform.Translate(Vector2.right * groundHorizontalLenght * 2);
	}
}