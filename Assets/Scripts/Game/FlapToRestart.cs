﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlapToRestart : MonoBehaviour 
{
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space))
        {
            ReiniciarJuego();
        }	
	}
	
	void ReiniciarJuego()
    {
        string nombreEscena = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(nombreEscena);
    }
}