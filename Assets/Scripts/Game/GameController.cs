﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public static GameController instance;

    [SerializeField] GameObject gameOverText;
    public bool gameOver;
    public float scrollSpeed = -1.5f;
	[SerializeField] Text scoreText; 

	private int score = 0;

    void Awake()
    {
        if (GameController.instance == null)
        {
            GameController.instance = this;
        }
        else if (GameController.instance)
        {
            Destroy(gameObject);
            Debug.LogWarning("GameController ha sido instanciado por segunda vez. Esto no debería pasar");
        }
    }

    // Use this for initialization
    void Start()
    {
        LimitarFPS();
    }

    public void FinnishGame()
    {
        gameOverText.SetActive(true);
        gameOver = true;
    }

	public void BirdScore()
	{
		if (gameOver) return;
		
		score++;
		scoreText.text = "Score: " + score;
        SoundSystem.instance.PlayCoin();
	}

    //Intentar Limitar Fotogramas por segundo
    private void LimitarFPS()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }

    private void OnDestroy()
    {
        if (GameController.instance == this)
        {
            GameController.instance = null;
        }
    }
}