﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    private bool isDead;
    private Rigidbody2D rb2D;
    [SerializeField] private float upForce = 200f;
	private Animator anim;

    RotateBird rotateBird;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
        rotateBird = GetComponent<RotateBird>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (isDead) return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fly();
        }
    }

    private void OnCollisionEnter2D()
    {
        BirdDie();
    }

    private void BirdDie()
    {
        isDead = true;
        anim.SetTrigger("Die");
        rotateBird.enabled = false;
        GameController.instance.FinnishGame();
        rb2D.velocity = Vector2.zero;
        SoundSystem.instance.PlayHit();
    }

    //Se agrega Vector2.zero para evitar acumular velocidad vertical por cada input
    private void Fly()
    {
        rb2D.velocity = Vector2.zero;
        rb2D.AddForce(Vector2.up * upForce);
        anim.SetTrigger("Flap");
        SoundSystem.instance.PlayFlap();
    }
}