﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPool : MonoBehaviour
{
    public int columnPoolSize = 5;
    public GameObject columnaPrefab;

    public float columnMin = 2.9f;
    public float columnMax = 1.4f;
    private float spawnXposition = 10f;

    private GameObject[] columnas;
    private Vector2 ObjectPoolPosition = new Vector2(-14, 0);

    private float timeSinceLastSpawned;
    public float spawnRate;

    private int currentColumn = 0;


    private void Start()
    {
        columnas = new GameObject[columnPoolSize];

        for (int i = 0; i < columnPoolSize; i++)
        {
            columnas[i] = Instantiate(columnaPrefab, ObjectPoolPosition, Quaternion.identity);
        }

        ColocarPrimeraColumna();
    }

    private void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;

        if (!GameController.instance.gameOver && timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0f;
            float spawnYposition = Random.Range(columnMin, columnMax);
            columnas[currentColumn].transform.position = new Vector2(spawnXposition, spawnYposition);

            currentColumn++;
            if (currentColumn >= columnPoolSize)
            {
                currentColumn = 0;
            }
        }
    }

    private void ColocarPrimeraColumna()
    {
        /* Se inicializa en 1 para no contar la columna
        que se inicializa en 0 */
        currentColumn = 1;
        
        //Colocar primera columna sin esperar tiempo de aparición
        float spawnYposition = Random.Range(columnMin, columnMax);
        columnas[0].transform.position = new Vector2 (spawnXposition, spawnYposition);
    }
}