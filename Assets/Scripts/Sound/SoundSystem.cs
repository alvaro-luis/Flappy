﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSystem : MonoBehaviour 
{
	public static SoundSystem instance;
	// public AudioClip audioClipCoin;
	// public AudioClip audioClipFlap;
	// public AudioClip audioClipHit;

	public AudioSource audioSourceCoin;
	public AudioSource audioSourceFlap;
	public AudioSource audioSourceHit;

	//public AudioSource audioSource;

	private void Awake()
	{
		if (SoundSystem.instance == null)
		{
			SoundSystem.instance = this;
		}else if(SoundSystem.instance != this)
		{
			Destroy(gameObject);
		}
	}

	public void PlayCoin()
	{
		//PlayAudioClip(audioClipCoin);
		audioSourceCoin.Play();
	}

	public void PlayFlap()
	{
		//PlayAudioClip(audioClipFlap);
		audioSourceFlap.Play();
	}

	public void PlayHit()
	{
		//PlayAudioClip(audioClipHit);
		audioSourceHit.Play();
	}

	// public void PlayAudioClip(AudioClip audioClip)
	// {
	// 	audioSource.clip = audioClip;
	// 	audioSource.Play();
	// }

	private void OnDestroy()
	{
		if (SoundSystem.instance == this)
		{
			SoundSystem.instance = null;
		}
	}
}